# Sofaraum Client

[![Build Status](https://drone.kolaente.de/api/badges/sofaraum/client/status.svg)](https://drone.kolaente.de/sofaraum/client)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](LICENSE)
[![Download](https://img.shields.io/badge/download-latest-brightgreen.svg)](https://storage.kolaente.de/minio/sofaraum-client/)
[![Go Report Card](https://goreportcard.com/badge/git.kolaente.de/sofaraum/client)](https://goreportcard.com/report/git.kolaente.de/sofaraum/client)

A client which looks for wifi devices to then send an aggregated number to the Sofaraum Server.

**Note:** This client need `aircrack-ng` to function properly. The only thing this client does is parsing dumps from 
`aircrack-ng` and sending them away.

## Build from source

This Project uses Go modules, so you'll need at least `go 1.11`.

As a typical go project, this one comes with all its depencencies, so all you have to do is running 

```bash
make build
```

and you should have a ready-to-use binary.

If you want to compile for Linux (arm included), Windows and MacOS, run

```bash
make release
```

This will create all binaries in `dist/`.