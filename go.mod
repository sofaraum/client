module git.kolaente.de/sofaraum/client

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/client9/misspell v0.3.4
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835
	github.com/gordonklaus/ineffassign v0.0.0-20180909121442-1003c8bd00dc
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/prometheus/client_golang v0.9.2
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	golang.org/x/crypto v0.0.0-20190506204251-e1dfcc566284
	golang.org/x/lint v0.0.0-20181026193005-c67002cb31c3
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
	golang.org/x/sys v0.0.0-20190507053917-2953c62de483 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190506145303-2d16b83fe98c // indirect
	src.techknowlogick.com/xgo v0.0.0-20190415010919-1502273fab15
)
