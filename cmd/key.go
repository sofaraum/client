//      Sofaraum client is the client software which collects statistics about
//      wifi devices nearby and then sends them to the Sofaraum Server.
//      Copyright (c) 2019.
//
//      This program is free software: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"git.kolaente.de/sofaraum/client/pkg/key"
	"github.com/spf13/cobra"
)

var keyCmd = &cobra.Command{
	Use:   "keygen",
	Short: "Generates an ed25519 public and private key pair used to sign the communication with the server.",
	Long: `Generates an ed25519 public and private key pair used to sign the communication with the server and saves it in the current directory.
The public key needs to be saved on the server seperatly.`,
	Run: func(cmd *cobra.Command, args []string) {
		key.GenKey()
	},
}

func init() {
	rootCmd.AddCommand(keyCmd)
}
