//     Sofaraum client is the client software which collects statistics about
//     wifi devices nearby and then sends them to the Sofaraum Server.
//     Copyright (c) 2018.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

// Version sets the version. Will be overwritten by drone or when building with the makefile
var Version = "0.1"

// TheSofa holds a fancy ascii art logo
var TheSofa = `
       ___.--------'´´´´´´:´´´´´´'--------.___
      (   |               :               |   )
       \ ,;,,,            :               |  /
        |\%%%%\___________:__________/~~~~~/|
       / ,\%%%%\          |         / @*@ /, \
      /_ / ´´´´´          |         ~~~~~~ \ _\	
     (@l)                 |                 (@l)
      ||__________________|__________________||
      ||_____________________________________||
     /_|_____________________________________|_\

  =================================================
    Sofaraum Client Version ` + Version + ` 

    Copyright (C) 2018 K. Langenberg
    This program comes with ABSOLUTELY NO WARRANTY
    This is free software, and you are welcome 
    to redistribute it under certain conditions
  =================================================`

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Shows version information",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(TheSofa)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
