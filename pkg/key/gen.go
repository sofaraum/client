//      Sofaraum client is the client software which collects statistics about
//      wifi devices nearby and then sends them to the Sofaraum Server.
//      Copyright (c) 2019.
//
//      This program is free software: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.  If not, see <https://www.gnu.org/licenses/>.

package key

import (
	"encoding/hex"
	"golang.org/x/crypto/ed25519"
	"io/ioutil"
	"log"
)

func GenKey() {
	pk, sk, err := ed25519.GenerateKey(nil)
	if err != nil {
		log.Fatalf("Could not generate key: %s", err)
	}

	err = ioutil.WriteFile("sofa-pub.key", pk, 0600)
	if err != nil {
		log.Fatalf("Could not write public key: %s", err)
	}

	err = ioutil.WriteFile("sofa-secret.key", sk, 0600)
	if err != nil {
		log.Fatalf("Could not write secret key: %s", err)
	}

	log.Println("Keys were generated successfully.")
	log.Printf("Public key (hex-encoded string): %s", hex.EncodeToString(pk))
}
