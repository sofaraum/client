//     Sofaraum client is the client software which collects statistics about
//     wifi devices nearby and then sends them to the Sofaraum Server.
//     Copyright (c) 2018.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

package config

const (
	// TheClientCSVHeader is the header of the csv file where aircrack-ng starts putting infos about the clients
	TheClientCSVHeader = `Station MAC, First time seen, Last time seen, Power, # packets, BSSID, Probed ESSIDs`
	// SecondsUntilInactive specifies the seconds until a client is considered inactive
	SecondsUntilInactive = 120
	// UpdateSecondsInterval is how often the currently active clients should be processed and sent to the server
	UpdateSecondsInterval = 2
)
