//      Sofaraum client is the client software which collects statistics about
//      wifi devices nearby and then sends them to the Sofaraum Server.
//      Copyright (c) 2019.
//
//      This program is free software: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.  If not, see <https://www.gnu.org/licenses/>.

package processing

import "fmt"

// ListenAndSendMetrics listens on active clients in the channel and sends these to the server
func ListenAndSendMetrics() {
	for {
		clients := <-ActiveClients
		fmt.Println(clients)

		// Sign the request

		// Send it to the server
	}
}
