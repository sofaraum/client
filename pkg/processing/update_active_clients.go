//     Sofaraum client is the client software which collects statistics about
//     wifi devices nearby and then sends them to the Sofaraum Server.
//     Copyright (c) 2018.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

package processing

import (
	"git.kolaente.de/sofaraum/client/pkg/config"
	"github.com/spf13/viper"
	"time"
)

// UpdateActiveClients counts all clients and sends the number to the server. Meant to run inside a goroutine.
func UpdateActiveClients() {
	for {
		clients := ParseCSVDumps(viper.GetString("daemon.csvlocation"))

		var activeClients int64
		for _, c := range clients {
			// fmt.Println(fmt.Sprintf("Mac: %s | First seen: %s | Last seen: %s | Power: %d | Packets: %d | Active: %t", c.MACAdress, c.FirstSeen.String(), c.LastSeen.String(), c.Power, c.Packets, c.isActive()))
			if c.IsActive() {
				activeClients++
			}
		}
		ActiveClients <- activeClients
		//fmt.Println("Active Clients:", activeClients)
		//fmt.Println("Total Clients:", len(clients))

		time.Sleep(config.UpdateSecondsInterval * time.Second)
	}
}
