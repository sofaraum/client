//      Sofaraum client is the client software which collects statistics about
//      wifi devices nearby and then sends them to the Sofaraum Server.
//      Copyright (c) 2018.
//
//      This program is free software: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.  If not, see <https://www.gnu.org/licenses/>.

package processing

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// RecordMetrics is the function which puts the active clients in the metrics endpoints
func RecordMetrics() {
	go func() {
		for {
			activeClients.Set(float64(<-ActiveClients))
		}
	}()
}

var (
	activeClients = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "sofa_active_clients",
		Help: "The currently active clients on this node",
	})
)
