//     Sofaraum client is the client software which collects statistics about
//     wifi devices nearby and then sends them to the Sofaraum Server.
//     Copyright (c) 2018.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

package processing

import (
	"github.com/spf13/viper"
	"log"
	"os/exec"
)

// RunAirodumpNG does what it says
func RunAirodumpNG(stop chan bool) {
	err := exec.Command("/bin/bash", []string{"-c", "airodump-ng " + viper.GetString("daemon.wifidevice") + " -w " + viper.GetString("daemon.csvlocation") + "/dump --output-format csv"}...).Run()
	if err != nil {
		log.Println("Could not run airodump-ng. Please make sure it is installed and you have sufficient permissions. ", err)
		stop <- true
	}
}
