//     Sofaraum client is the client software which collects statistics about
//     wifi devices nearby and then sends them to the Sofaraum Server.
//     Copyright (c) 2018.
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

package models

import (
	"git.kolaente.de/sofaraum/client/pkg/config"
	"time"
)

// WifiClient holds informations about a wifi client
type WifiClient struct {
	MACAdress string
	FirstSeen time.Time
	LastSeen  time.Time
	Power     int64
	Packets   int64
}

// IsActive checks whether or the client has been seen within a configurable amount of time
func (c *WifiClient) IsActive() bool {
	// Should normally not be set here, should take the system time -> see below
	current := time.Date(2018, 11, 14, 14, 31, 0, 0, &time.Location{})
	diff := current.Sub(c.LastSeen)

	//diff := time.Since(c.LastSeen)
	if diff < config.SecondsUntilInactive*time.Second {
		return true
	}

	return false
}
