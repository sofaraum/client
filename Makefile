DIST := dist
IMPORT := git.kolaente.de/sofaraum/client

SED_INPLACE := sed -i

ifeq ($(OS), Windows_NT)
	EXECUTABLE := sofa.exe
else
	EXECUTABLE := sofa
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		SED_INPLACE := sed -i ''
	endif
endif

GOFILES := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")
GOFMT ?= gofmt -s

GOFLAGS := -v -mod=vendor
EXTRA_GOFLAGS ?=

LDFLAGS := -X "cmd.Version=$(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')" -X "main.Tags=$(TAGS)"

PACKAGES ?= $(filter-out git.kolaente.de/sofaraum/client/integrations,$(shell go list ./... | grep -v /vendor/))
SOURCES ?= $(shell find . -name "*.go" -type f)

TAGS ?=

TMPDIR := $(shell mktemp -d 2>/dev/null || mktemp -d -t 'sofaclient-temp')

ifeq ($(OS), Windows_NT)
	EXECUTABLE := sofa.exe
else
	EXECUTABLE := sofa
endif

ifneq ($(DRONE_TAG),)
	VERSION ?= $(subst v,,$(DRONE_TAG))
else
	ifneq ($(DRONE_BRANCH),)
		VERSION ?= $(subst release/v,,$(DRONE_BRANCH))
	else
		VERSION ?= master
	endif
endif

VERSION := $(shell echo $(VERSION) | sed 's/\//\-/g')

.PHONY: all
all: build

.PHONY: clean
clean:
	go clean ./...
	rm -rf $(EXECUTABLE) $(DIST) $(BINDATA)

.PHONY: test
test:
	go test -cover -coverprofile cover.out $(PACKAGES)
	go tool cover -html=cover.out -o cover.html

.PHONY: lint
lint:
	@hash golint > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) golang.org/x/lint/golint; \
	fi
	for PKG in $(PACKAGES); do golint -set_exit_status $$PKG || exit 1; done;

.PHONY: fmt
fmt:
	$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check:
	# get all go files and run go fmt on them
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

.PHONY: install
install: $(wildcard *.go)
	go install -v -tags '$(TAGS)' -ldflags '-s -w $(LDFLAGS)'

.PHONY: build
build: $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES)
	go build $(GOFLAGS) $(EXTRA_GOFLAGS) -tags '$(TAGS)' -ldflags '-s -w $(LDFLAGS)' -o $@

.PHONY: release
release: release-dirs release-windows release-linux release-darwin release-check

.PHONY: release-dirs
release-dirs:
	mkdir -p $(DIST)/binaries

.PHONY: release-windows
release-windows:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '-linkmode external -extldflags "-static" $(LDFLAGS)' -targets 'windows/*' -out vikunja-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-linux
release-linux:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '-linkmode external -extldflags "-static" $(LDFLAGS)' -targets 'linux/*' -out vikunja-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-darwin
release-darwin:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '$(LDFLAGS)' -targets 'darwin/*' -out vikunja-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-check
release-check:
	cd $(DIST)/binaries; $(foreach file,$(wildcard $(DIST)/binaries/$(EXECUTABLE)-*),sha256sum $(notdir $(file)) > $(notdir $(file)).sha256;)

.PHONY: misspell-check
misspell-check:
	@hash misspell > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) github.com/client9/misspell/cmd/misspell; \
	fi
	for S in $(GOFILES); do misspell -error $$S || exit 1; done;

.PHONY: ineffassign-check
ineffassign-check:
	@hash ineffassign > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) github.com/gordonklaus/ineffassign; \
	fi
	for S in $(GOFILES); do ineffassign $$S || exit 1; done;

.PHONY: gocyclo-check
gocyclo-check:
	@hash gocyclo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) github.com/fzipp/gocyclo; \
	fi
	for S in $(GOFILES); do gocyclo -over 14 $$S || exit 1; done;
